package services

import (
	"u_user_1/u_go_api_gateway/config"
	"u_user_1/u_go_api_gateway/genproto/user_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	UserService() user_service.UserServiceClient
	HobbiService() user_service.HobbiesClient
}

type grpcClients struct {
	userService  user_service.UserServiceClient
	hobbiService user_service.HobbiesClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// User Service...
	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		userService: user_service.NewUserServiceClient(connUserService),
		hobbiService: user_service.NewHobbiesClient(connUserService),
	}, nil
}

func (g *grpcClients) UserService() user_service.UserServiceClient {
	return g.userService
}

func (g *grpcClients) HobbiService() user_service.HobbiesClient {
	return g.hobbiService
}

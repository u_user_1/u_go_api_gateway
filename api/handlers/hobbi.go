package handlers

import (
	"context"
	"u_user_1/u_go_api_gateway/api/http"
	"u_user_1/u_go_api_gateway/api/models"
	"u_user_1/u_go_api_gateway/genproto/user_service"
	"u_user_1/u_go_api_gateway/pkg/helper"
	"u_user_1/u_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateHobbi godoc
// @ID create_hobbi
// @Router /hobbi [POST]
// @Summary Create Hobbi
// @Description  Create Hobbi
// @Tags Hobbi
// @Accept json
// @Produce json
// @Param profile body user_service.CreateHobbi true "CreateHobbiRequestBody"
// @Success 200 {object} http.Response{data=user_service.Hobbi} "GetHobbiBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateHobbi(c *gin.Context) {

	var hobbi user_service.CreateHobbi

	err := c.ShouldBindJSON(&hobbi)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.HobbiService().Create(
		c.Request.Context(),
		&hobbi,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetHobbiByID godoc
// @ID get_hobbi_by_id
// @Router /hobbi/{id} [GET]
// @Summary Get Hobbi  By ID
// @Description Get Hobbi  By ID
// @Tags Hobbi
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.Hobbi} "UserBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetHobbiByID(c *gin.Context) {

	userID := c.Param("id")

	if !util.IsValidUUID(userID) {
		h.handleResponse(c, http.InvalidArgument, "Hobbi id is an invalid uuid")
		return
	}

	resp, err := h.services.HobbiService().GetByID(
		context.Background(),
		&user_service.HobbiPrimaryKey{
			Id: userID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetHobbiList godoc
// @ID get_hobbi_list
// @Router /hobbi [GET]
// @Summary Get Hobbi s List
// @Description  Get Hobbi s List
// @Tags Hobbi
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=user_service.GetListHobbiResponse} "GetAllHobbiResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetHobbiList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.HobbiService().GetList(
		context.Background(),
		&user_service.GetListHobbiRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateHobbi godoc
// @ID update_hobbi
// @Router /hobbi/{id} [PUT]
// @Summary Update Hobbi
// @Description Update Hobbi
// @Tags Hobbi
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.UpdateHobbi true "UpdateHobbiRequestBody"
// @Success 200 {object} http.Response{data=user_service.Hobbi} "Hobbi data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateHobbi(c *gin.Context) {

	var hobbi user_service.UpdateHobbi

	hobbi.Id = c.Param("id")

	if !util.IsValidUUID(hobbi.Id) {
		h.handleResponse(c, http.InvalidArgument, "user id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&hobbi)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.HobbiService().Update(
		c.Request.Context(),
		&hobbi,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchHobbi godoc
// @ID patch_hobbi
// @Router /hobbi/{id} [PATCH]
// @Summary Patch Hobbi
// @Description Patch Hobbi
// @Tags Hobbi
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=user_service.Hobbi} "Hobbi data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchHobbi(c *gin.Context) {

	var updatePatchHobbi models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchHobbi)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchHobbi.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchHobbi.ID) {
		h.handleResponse(c, http.InvalidArgument, "Hobbi id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchHobbi.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.HobbiService().UpdatePatch(
		c.Request.Context(),
		&user_service.UpdatePatchHobbi{
			Id:     updatePatchHobbi.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteHobbi godoc
// @ID delete_hobbi
// @Router /hobbi/{id} [DELETE]
// @Summary Delete Hobbi
// @Description Delete Hobbi
// @Tags Hobbi
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Hobbi data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteHobbi(c *gin.Context) {

	hobbiId := c.Param("id")

	if !util.IsValidUUID(hobbiId) {
		h.handleResponse(c, http.InvalidArgument, "Hobbi id is an invalid uuid")
		return
	}

	resp, err := h.services.HobbiService().Delete(
		c.Request.Context(),
		&user_service.HobbiPrimaryKey{Id: hobbiId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
